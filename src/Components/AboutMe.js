import React, { Component } from 'react';
import img1 from '../image/шифт.jpg';
import './aboutMe.css'
import Comment from "./Comment"


export default class AboutMe extends Component {
    render() {
        return (
            <div className="row">
                        <img
                            className="img-fluid"
                            width="900px"
                            src={img1}
                            align="left"
                            alt="school-IT" />
                <div className="aboutMe" >
                    <h3 className="mt-5 mb-5" align="center">Обо мне</h3>
                    <ul className="list">
                        <li className="mt-5">Обучаюсь на 3 курсе факультета прикладной математике и информатике; </li>
                        <li className="mt-5">В данный момент прохожу курсы по Frontend-разработки и web-дизайна от WayUp;</li>
                        <li className="mt-5">Также проходила обучение в школе информационных и финансовых технологий в
                        секциях Тестирование (2020) и Frontend (2021);</li>
                        <li className="mt-5">Зимой я люблю кататься на горных лыжах, а летом путешествовать по России, 
                        особенно заезжать в самые отдаленные места Алтая.</li>
                    </ul>
                </div>
                <Comment />
            </div>
        );
    }
}
