import React, { Component } from 'react'
import Carousel from 'react-bootstrap/Carousel'
import img1 from '../image/IMG_3795.jpg'
import img2 from '../image/IMG_3708.jpg'
import img3 from '../image/IMG_3977.jpg'
import './first.css'
import AboutMe from './AboutMe'


export default class First extends Component {
    render() {
        return(
            <div className="first">
                <Carousel className="pagination">
                    <Carousel.Item>
                        <img 
                            className="img-fluid"
                            width="980px"
                            src={img1}
                            align="right"
                            alt="It's me"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img 
                            className="img-fluid"
                            width="980px"
                            src={img2}
                            align="right"
                            alt="It's me"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img 
                            className="img-fluid"
                            width="980px"
                            src={img3}
                            align="right"
                            alt="It's me"
                        />
                    </Carousel.Item>
                
                </Carousel>
                <div className="header">
                    <h1 className="border-top border-dark mb-1">Бортникова Анна</h1>
                    <p className="how mb-1">Веб-разработчик</p>
                    <p className="howhow"> UX/UI дизайнер</p>
                </div>
                <AboutMe />           
            </div>
        )
    }
}