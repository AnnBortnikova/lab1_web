import { useState } from 'react'
import { Container, Form } from 'react-bootstrap';
import del from '../image/delete.png'

function Comment() {

    const [comments, setComment] = useState([])
    const [userInput, setUserInput] = useState('')

    const addComment = (userInput) => {
        if(userInput) {
          const newItem = {
            id: Math.random().toString(36).substr(2,9),
            task: userInput,
            complete: false
          }
          setComment([...comments, newItem])
        }
    }

    const removeComment = (id) => {
        setComment([...comments.filter((comment) => comment.id !== id)])
    }

    const handleToggle = (id) => {
        setComment([
          ...comments.map((comment) => 
          comment.id === id ? { ...comment, complete: !comment.complete } : {...comment }
          )
        ])
    }
    const handleChange = (e) => {
        setUserInput(e.currentTarget.value)
    }
    
    const handleSubmit = (e) => {
        e.preventDefault()
        addComment(userInput)
        setUserInput("")
    }

    const handleKeyPress = (e) => {
        if(e.key === "Enter") {
            handleSubmit(e)
        }
    }

        return (
            <Container>
                <div className="row">
                    <div className="col-lg-12">
                        <h2 className="text-center mt-5">Комментарии {comments.length}:</h2>
                    </div>    
                    <div>
                        <div id="comment-field"></div>
                    </div>
                    <div className="col-lg-6">
                        <Form onSubmit={handleSubmit}>
                            {/* <div className="form-group">
                                Имя:
                                <input onChange={handleChange} onKeyDown={handleKeyPress} 
                                    type="name" className="form-control" id="comment-name"  placeholder="Введите свое имя">
                                </input>
                            </div> */}
                            <div className="form-group">
                                Комментарий:
                                <textarea onChange={handleChange} onKeyDown={handleKeyPress} 
                                type="password" className="form-control" id="comment-body" placeholder="Введите комментарий">
                            </textarea>
                            </div>
                            <div className="form-group form-check text-right">
                                <button addComment={addComment} type="submit" id="comment-add" className="btn btn-primary">Добавить комментарий</button>
                            </div>
                        </Form>
                    </div>
                    <div className="col-lg-6">
                        {comments.map((comment) => {
                            return(
                                <div key={comment.id} className="border border-dark ml-5 mr-5 mt-3 mb-5 p-3 row justify-content-between">
                                    <div onClick={() => handleToggle(comment.id)}>
                                        {comment.task}
                                    </div>
                                    <div className="col-1" style={{width: "18px"}} onClick={() => removeComment(comment.id)}>
                                        <img src={del} style={{width: "18px"}}/>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </Container>
        )
}

export default Comment;

