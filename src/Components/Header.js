import React, { Component } from 'react'
import {Nav, Navbar, Container,} from 'react-bootstrap';
import logo from "./logo.png";
import instagram from "./instagram.svg";
import vk from "./vk.svg";
import './Header.css';
import CarouselBox from './First';

export default class Header extends Component {
    render() {
        return (
            <>
            <Navbar collapseOnSelect expand="md" variant="light" className="d-flex align-content-around flex-wrap" id="header">
                <Container>
                    <Navbar.Brand href="/" className="logo">
                        <img
                            src={logo}
                            height="60"
                            width="60"
                            className="d-inline-block align-top"
                            alt="Logo"
                        />  
                        <p className="name mt-2"> Сайт Бортниковой Анна об изучении React JS</p>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link href="https://www.instagram.com/ann_bortnikova/" className="icon">
                            <img
                                src={instagram}
                                height="30"
                                width="30"
                                className="d-inline-block mt-2 ml-5"
                                alt="Instagram"
                            />
                            </Nav.Link>
                            <Nav.Link href="https://vk.com/ann_bortnikova" className="icon">
                            <img
                                src={vk}
                                height="40"
                                width="40"
                                className="d-inline-block mt-1 ml-5"
                                alt="Vk"
                            />
                            </Nav.Link>
                        </Nav>
                        <p className="data mt-2">anna_bortnik@mail.ru +7 913 372 29 57</p>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <CarouselBox />
            </>
        )
    }
}